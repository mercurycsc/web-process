**This is no longer maintained!**

Instead see the [wiki in Django-Newproj](https://github.com/jbergantine/django-newproj-template/wiki/_pages).

---

# MercuryCSC Web Process

## Table of Contents

1. [Guidelines for Development.md](https://bitbucket.org/mercurycsc/web-process/src/master/1_Guidelines_for_Development.md)
2. [Going Live.md](https://bitbucket.org/mercurycsc/web-process/src/master/2_Going_Live.md)
3. [Go Live Checklist.md](https://bitbucket.org/mercurycsc/web-process/src/master/3_Go_Live_Checklist.md)