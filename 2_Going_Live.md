# Going Live

## Go-Live and Main Line Branch Merges

It is the sole responsibility of the Interactive Producer to allow changes made to be pushed to a production server (a production server here is distinguished from a staging or development server).

The Interactive Producer will communicate changes made to a production server to the accounts team.

A main line Git branch for each project called "master" will be created. That will be the sole branch allowed to deploy to a live server.

The Interactive Producer will be solely responsible for merging other branches onto the main line branch.

The Interactive Producer will be solely responsible for deploying changes to the production server once a site is live.

In merging a branch onto the main line branch the qualifications defined in [the go-live checklist](https://bitbucket.org/mercurycsc/web-process/src/master/3_Go_Live_Checklist.md) serve as guidelines to be met.

## Spelling

Before a site can go live, any content within the page created by MercuryCSC must be spell checked and approved by the accounts team. This spell check must happen in-browser and changes must be verified by the accounts team before the site can go live. This should be the **last** thing that happens before the site goes live given the nature of markup and its relationship to the content.

Spelling of meta data and other non-visible data will be done by the Interactive Producer immediately before go-live or after revisions to that data. A document containing the meta data for each page (including the Title, Description, Keywords, Facebook Graph Title and Facebook Graph Description) will be provided to the accounts team during the content development stage for sign off.

The Interactive Producer will work with the accounts team to establish for the project how and whether client-generated content will be reviewed.

For changes made to an existing site, it will be the discretion of the Interactive Producer to decide what level of content review needs to happen.
